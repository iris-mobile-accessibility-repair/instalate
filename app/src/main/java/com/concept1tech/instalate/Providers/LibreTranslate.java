/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.net.Uri;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;

import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.MySpannableStringBuilder;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LibreTranslate extends Provider {

    // order is important
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "en",
                /*[1] FRENCH*/           "fr",
                /*[2] CZECH*/            null,
                /*[3] POLISH*/           "pl",
                /*[4] SLOVAK*/           null,
                /*[5] GERMAN*/           "de",
                /*[6] HUNGARIAN*/        null,
                /*[7] DUTCH*/            null,
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          "ru",
                /*[10] SPANISH*/         "es",
                /*[11] SWEDISH*/         null,
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       null,
                /*[14] ITALIAN*/         "it",
                /*[15] FINNISH*/         null,
                /*[16] DANISH*/          null,
                /*[17] PORTUGUESE*/      "pt",
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       null,
                /*[20] ROMANIAN*/        null,
                /*[21] LATIN*/           null,
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         "tr",
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           null,
                /*[27] CHINESE*/         "zh",
                /*[28] JAPANESE*/        "ja",
                /*[29] SLOVENE*/         null,
                /*[30] LITHUANIAN*/      null,
                /*[31] LATVIAN*/         null,
                /*[32] ESTONIAN*/        null,
                /*[33] MALTESE*/         null,
                /*[34] MALAGASY*/        null,
                /*[35] INDONESIAN*/      "id",
                /*[36] ARABIC*/          "ar",
                /*[37] HINDI*/           "hi",
                /*[38] IRISH*/           "ga",
                /*[39] KOREAN*/          "ko",
                /*[40] UKRAINIAN*/       null,
                /*[41] VIETNAMESE*/      "vi"
    };


    public LibreTranslate(String label, String id, int icon) {
        super(label, id, icon);

        Boolean[][][] combinations = new Boolean[3][][];
        combinations[0] = buildBiDirMat();
        combinations[1] = buildForwardDirMat();
        combinations[2] = combinations[1];
        setCombinations(combinations);

//        isTest();
    }


    @Override
    public void preProcess(Context c, TranslationData data) {
        PageRequest request = data.getRequest();
        String s = data.getCleanedText();

        setPageRequestDefaults(request);
        request.setMethod(PageRequest.POST);
        request.getHTTPHeaders().setHeader("accept", "application/json");
        request.getHTTPHeaders().setHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setUri(Uri.parse("https://translate.argosopentech.com/translate"));
        data.setBrowserUrl(Uri.parse("https://translate.argosopentech.com"));

        Map<String, String> payload = new HashMap<>();
        payload.put("q", s);
        payload.put("source", mLangSpecs[data.getSrcLang().getIdx()]);
        payload.put("target", mLangSpecs[data.getTgtLang().getIdx()]);
        payload.put("format", "text");
        request.setDataUrlEncoded(payload);
    }
    @Override
    public void postProcess(Context c, TranslationData data) {
        int leadingMargin = c.getResources().getDimensionPixelOffset(R.dimen.sp_32);
        List<ParcelableCharSequence> tList = new ArrayList<>();
        PageResponse response = data.getResponse();

        try {
            JSONObject root = new JSONObject(response.get());
            tList.add(new ParcelableCharSequence(data.getSrcLang().getName() + ":"));
            tList.add(new ParcelableCharSequence(new MySpannableStringBuilder(data.getCleanedText(),
                        new LeadingMarginSpan.Standard(leadingMargin), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)));
            tList.add(new ParcelableCharSequence(""));
            tList.add(new ParcelableCharSequence(data.getTgtLang().getName() + ":"));
            tList.add(new ParcelableCharSequence(new MySpannableStringBuilder(root.optString("translatedText"),
                        new LeadingMarginSpan.Standard(leadingMargin), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        data.setTranslationList(tList);
        response.set("");
    }

    @Override
    public String cleanText(String s) {
        return s.trim();
    }

    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);
        return mat;
    }

    private Boolean[][] buildForwardDirMat() {
        Boolean[] langSpecsB = ArrayUtils.toBool(mLangSpecs);
        Boolean[][] mat = ArrayUtils.arrayToSymMatrix(langSpecsB);
        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private void isTest() {
        ArrayUtils.logMatrices(getCombinations(), mLangSpecs, mLangSpecs, 4);
    }
}
