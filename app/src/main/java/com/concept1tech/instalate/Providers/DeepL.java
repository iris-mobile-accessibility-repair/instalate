/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;

import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.unn.ArrayUtils;

public class DeepL extends Provider {

    private final String[] mBaseUrls = {
                null,
                "https://www.deepl.com/translator#%2$s/%3$s/%1$s",
                "https://www.deepl.com/translator#%3$s/%2$s/%1$s",
    };

    // order is important
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "en",
                /*[1] FRENCH*/           "fr",
                /*[2] CZECH*/            "cs",
                /*[3] POLISH*/           "pl",
                /*[4] SLOVAK*/           "sk",
                /*[5] GERMAN*/           "de",
                /*[6] HUNGARIAN*/        "hu",
                /*[7] DUTCH*/            "nl",
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          "ru",
                /*[10] SPANISH*/         "es",
                /*[11] SWEDISH*/         "sv",
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       null,
                /*[14] ITALIAN*/         "it",
                /*[15] FINNISH*/         "fi",
                /*[16] DANISH*/          "da",
                /*[17] PORTUGUESE*/      "pt",
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       "bg",
                /*[20] ROMANIAN*/        "ro",
                /*[21] LATIN*/           null,
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         null,
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           "el",
                /*[27] CHINESE*/         "zh",
                /*[28] JAPANESE*/        "ja",
                /*[29] SLOVENE*/         "sl",
                /*[30] LITHUANIAN*/      "lt",
                /*[31] LATVIAN*/         "lv",
                /*[32] ESTONIAN*/        "et",
                /*[33] MALTESE*/         null,
                /*[34] MALAGASY*/        null,
                /*[35] INDONESIAN*/      null,
                /*[36] ARABIC*/          null,
                /*[37] HINDI*/           null,
                /*[38] IRISH*/           null,
                /*[39] KOREAN*/          null,
                /*[40] UKRAINIAN*/       null,
                /*[41] VIETNAMESE*/      null
    };

    public DeepL(String label, String id, int icon) {
        super(label, id, icon);

        Boolean[][][] combinations = new Boolean[3][][];
        combinations[0] = buildBiDirMat();
        combinations[1] = buildForwardDirMat();
        combinations[2] = combinations[1];
        setCombinations(combinations);
        setBrowserOnly(true);

//        isTest();
    }


    @Override
    public void preProcess(Context c, TranslationData data) {
        data.setBrowserUrl(parseUri(mBaseUrls, data.getCleanedText(), mLangSpecs, data.getActiveLangs()));
    }
    @Override
    public void postProcess(Context c, TranslationData data) {

    }
    @Override
    public String cleanText(String s) {
        s = s.replaceAll("[/|]+", " ");  // remove some characters
        return s.trim();
    }

    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);
        return mat;
    }

    private Boolean[][] buildForwardDirMat() {
        Boolean[] langSpecsB = ArrayUtils.toBool(mLangSpecs);
        Boolean[][] mat = ArrayUtils.arrayToSymMatrix(langSpecsB);
        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private void isTest() {
        ArrayUtils.logMatrices(getCombinations(), mLangSpecs, mLangSpecs, 4);
    }
}
