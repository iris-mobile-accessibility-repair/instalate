/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import android.util.Log;

import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.MySpannableStringBuilder;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

public class Dictcc extends Provider {

    public static final String TAG = Dictcc.class.getSimpleName();

    private final String[] mBaseUrls = {"https://%2$s%3$s.dict.cc/?s=%1$s",      // bi-directional
                "https://%2$s-%3$s.dict.cc/?s=%1$s",        // forward directional
                "https://%3$s-%2$s.dict.cc/?s=%1$s"};       // backwards directional

    // order is important
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "en",
                /*[1] FRENCH*/           "fr",
                /*[2] CZECH*/            "cs",
                /*[3] POLISH*/           "pl",
                /*[4] SLOVAK*/           "sk",
                /*[5] GERMAN*/           "de",
                /*[6] HUNGARIAN*/        "hu",
                /*[7] DUTCH*/            "nl",
                /*[8] ALBANIAN*/         "sq",
                /*[9] RUSSIAN*/          "ru",
                /*[10] SPANISH*/         "es",
                /*[11] SWEDISH*/         "sv",
                /*[12] ICELANDIC*/       "is",
                /*[13] NORWEGIAN*/       "no",
                /*[14] ITALIAN*/         "it",
                /*[15] FINNISH*/         "fi",
                /*[16] DANISH*/          "da",
                /*[17] PORTUGUESE*/      "pt",
                /*[18] CROATIAN*/        "hr",
                /*[19] BULGARIAN*/       "bg",
                /*[20] ROMANIAN*/        "ro",
                /*[21] LATIN*/           "la",
                /*[22] ESPERANTO*/       "eo",
                /*[23] BOSNIAN*/         "bs",
                /*[24] TURKISH*/         "tr",
                /*[25] SERBIAN*/         "sr",
                /*[26] GREEK*/           "el",
                /*[27] CHINESE*/         null,
                /*[28] JAPANESE*/        null,
                /*[29] SLOVENE*/         null,
                /*[30] LITHUANIAN*/      null,
                /*[31] LATVIAN*/         null,
                /*[32] ESTONIAN*/        null,
                /*[33] MALTESE*/         null
    };

    public Dictcc(String label, String id, int icon) {
        super(label, id, icon);

        Boolean[][][] combinations = new Boolean[3][][];
        combinations[0] = buildBiDirMat();
        combinations[1] = buildForwardsMat();
        combinations[2] = buildBackwardsMat();
        setCombinations(combinations);

//        isTest();
    }

    @Override
    public void preProcess(Context c, TranslationData data) {
        PageRequest request = data.getRequest();
        String s = data.getCleanedText();
        setPageRequestDefaults(request);
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, data.getActiveLangs()));
    }
    @Override
    public void postProcess(Context c, TranslationData data) {
        int leadingMargin = c.getResources().getDimensionPixelOffset(R.dimen.sp_32);
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();
        Document doc = Jsoup.parse(response.get());

        // jsoup selector syntax: https://jsoup.org/apidocs/org/jsoup/select/Selector.html
        Elements tableRows = doc.select("tr[id^=tr]");  // get table rows that have an id attribute with a value beginning with "tr"

        for (Element row : tableRows) {
            Elements leftAndRight = row.select("td.td7nl");   // get elems with classname "td7nl" (should always be size 2)
            if (leftAndRight.size() < 2) {
                Log.w(TAG, "Row has less than 2 elements. Skipping...");
                continue;
            } else if (leftAndRight.size() > 2) {
                Log.w(TAG, "Row has more than 2 elements");
            }
            MySpannableStringBuilder builder = new MySpannableStringBuilder();
            for (int i = 0; i < 2; i++) {
                Element cell = leftAndRight.get(i);
                Elements allInCell = cell.select("a, var");    // get elements named "a" (has translated word) or "var"
                if (i == 0) {
                    builder.append(String.format("∙ %s\n", allInCell.text()));
                } else {
                    builder.append(allInCell.text(), new LeadingMarginSpan.Standard(leadingMargin), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            tList.add(new ParcelableCharSequence(builder));
        }

        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
    }

    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);

        Boolean[] en = ArrayUtils.toBool(mLangSpecs);
        ArrayUtils.setFromArray(mat, en, 0, 0, 0);   // en
        Boolean[] de = en.clone();
        ArrayUtils.setFromArray(mat, de, 0, 5, 0);   // de

        ArrayUtils.disjunctSymmetric(mat);
        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private Boolean[][] buildForwardsMat() {
        return buildBiDirMat();
    }

    private Boolean[][] buildBackwardsMat() {
        return buildBiDirMat();
    }

    private void isTest() {
        ArrayUtils.logMatrices(getCombinations(), mLangSpecs, mLangSpecs, 4);
    }
}
