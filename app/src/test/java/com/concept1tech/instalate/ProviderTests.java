/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.content.res.Resources;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;

import com.concept1tech.instalate.Providers.Beolingus;
import com.concept1tech.instalate.Providers.Dictcc;
import com.concept1tech.instalate.Providers.Gcide;
import com.concept1tech.instalate.Providers.Heinzelnisse;
import com.concept1tech.instalate.Providers.LibreTranslate;
import com.concept1tech.instalate.Providers.Linguee;
import com.concept1tech.instalate.Providers.WikDict;
import com.concept1tech.instalate.Providers.WiktionaryLinks;
import com.concept1tech.unn.NetworkUtils;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;


@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)   // suppress warning about missing manifest&resources
public class ProviderTests {

    private static final MockedStatic<Provider> sProvMock = mockStatic(Provider.class);
    private static final MockedStatic<App> sAppMock = mockStatic(App.class);

    private final TranslationDialogActivity mActMock = mock(TranslationDialogActivity.class);
    private final Context mCtxMock = mock(Context.class);
    private final Resources mResMock = mock(Resources.class);

    private final Beolingus mBeolingus = new Beolingus("", "Beolingus", 0);
    private final Dictcc mDictcc = new Dictcc("", "Dictcc", 0);
    private final Linguee mLinguee = new Linguee("", "Linguee", 0);
    private final WikDict mWikDict = new WikDict("", "WikDict", 0);
    private final WiktionaryLinks mWiktionaryLinks = new WiktionaryLinks("", "WiktionaryLinks", 0);
    private final Gcide mGcide = new Gcide("", "GCIDE", 0);
    private final Heinzelnisse mHeinzelnisse = new Heinzelnisse("", "Heinzelnisse", 0);
    private final LibreTranslate mLibreTranslate = new LibreTranslate("", "LibreTranslate", 0);

    private final MockedStatic.Verification mCallProviderGetById = () -> Provider.getById(any(), any());
    private final MockedStatic.Verification mCallAppGetActiveLangs = () -> App.getActiveLangsFromSPrefs(any());
    private final ArgumentCaptor<TranslationData> mArgCaptor = ArgumentCaptor.forClass(TranslationData.class);


    @Test
    public void shouldYieldTranslation_01() {
        shouldYieldTranslationBase(mWiktionaryLinks, "weird", "komisch", new String[]{"0", "1", "5"});
    }
    @Test
    public void shouldYieldTranslation_02() {
        shouldYieldTranslationBase(mWiktionaryLinks, "gefährlich", "dangerous", new String[]{"5", "1", "0"});
    }
    @Test
    public void shouldYieldTranslation_03() {
        shouldYieldTranslationBase(mWikDict, "erdmännchen", "meerkat", new String[]{"5", "0", "0"});
    }
    @Test
    public void shouldYieldTranslation_04() {
        shouldYieldTranslationBase(mWikDict, "dog", "犬", new String[]{"0", "0", "28"});
    }
    @Test
    public void shouldYieldTranslation_05() {
        shouldYieldTranslationBase(mBeolingus, "meerkats", "Erdmännchen", new String[]{"0", "0", "5"});
    }
    @Test
    public void shouldYieldTranslation_06() {
        shouldYieldTranslationBase(mBeolingus, "hallo", "Hello", new String[]{"5", "0", "0"});
    }
    @Test
    public void shouldYieldTranslation_07() {
        shouldYieldTranslationBase(mLinguee, "erdmännchen", "meerkat", new String[]{"5", "0", "0"});
    }
    @Test
    public void shouldYieldTranslation_08() {
        shouldYieldTranslationBase(mDictcc, "erdmännchen", "meerkat", new String[]{"5", "0", "0"});
    }
    @Test
    public void shouldYieldTranslation_09() {
        shouldYieldTranslationBase(mGcide, "proof", "evidence", new String[]{"0", "0", "0"});
    }
    @Test
    public void shouldYieldTranslation_10() {
        shouldYieldTranslationBase(mHeinzelnisse, "sein", "hans", new String[]{"5", "0", "13"});
    }
    @Test
    public void shouldYieldTranslation_11() {
        shouldYieldTranslationBase(mLibreTranslate, "солнце сияет", "Sonne scheint", new String[]{"9", "1", "5"});
    }


    // @AfterClass requires method to be static
    @AfterClass
    public static void tearDown() {
        // MockedStatic objects need to be closed (could also use try-with-resources statement)
        sProvMock.close();
        sAppMock.close();
    }


    private void shouldYieldTranslationBase(Provider provider, String wordToTransl, String wordTransl, String[] activeLanguages) {
        sProvMock.when(mCallProviderGetById).thenReturn(provider);
        sAppMock.when(mCallAppGetActiveLangs).thenReturn(activeLanguages);

        doReturn(mCtxMock).when(mActMock).getApplicationContext();
        doReturn(mResMock).when(mCtxMock).getResources();
        doReturn(0).when(mResMock).getDimensionPixelOffset(anyInt());
        doReturn(new String[200]).when(mResMock).getStringArray(anyInt());

        TranslationTask taskSpy = spy(new TranslationTask(mActMock));
        // redirect from AsyncTask.execute() to AsyncTask.OnPostExecute()
        // (we're not starting the async task, calling NetworkUtils.getPageSource() explicitly)
        Answer<String> callOnPostExecute = (InvocationOnMock invocation) -> {
            PageRequest request = invocation.getArgument(0, PageRequest.class);
            PageResponse response = NetworkUtils.getPageSource(request);
            taskSpy.onPostExecute(response);
            return null;
        };
        doAnswer(callOnPostExecute).when(taskSpy).execute(any(PageRequest.class));
        doReturn(true).when(taskSpy).hasInternetConnection(any(Context.class));


        taskSpy.executeTranslation(wordToTransl);


        verify(taskSpy).showActivity(mArgCaptor.capture());
        PageResponse response = mArgCaptor.getValue().getResponse();
        assertThat(response.hasErrors(), is(false));

        List<ParcelableCharSequence> tList = mArgCaptor.getValue().getTranslationList();
        String tString = translationListToString(tList);
        assertThat(tString, containsString(wordTransl));

        System.out.println("\n\n==================== " + provider.getId() + ": ====================\n");
        System.out.println(tString);
    }

    private String translationListToString(List<ParcelableCharSequence> tList) {
        StringBuilder builder = new StringBuilder();
        for (ParcelableCharSequence transl : tList) {
            CharSequence cs = transl.get();
            if (cs instanceof Spanned) {
                builder.append(leadingMarginSpanToString((Spanned) cs, "      "));
            } else {
                builder.append(cs);
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    private StringBuilder leadingMarginSpanToString(Spanned ss, String prependWith) {
        StringBuilder builder = new StringBuilder();
        String asString = ss.toString();
        LeadingMarginSpan[] lmSpans = ss.getSpans(0, ss.length(), LeadingMarginSpan.class);
        int lastSpanEnd = 0;
        for (LeadingMarginSpan lms : lmSpans) {
            int curSpanStart = ss.getSpanStart(lms);
            int curSpanEnd = ss.getSpanEnd(lms);
            String curString = asString.substring(curSpanStart, curSpanEnd);
            builder.append(asString.substring(lastSpanEnd, curSpanStart));
            builder.append(prependWith);
            builder.append(curString.replace("\n", "\n" + prependWith));
            lastSpanEnd = curSpanEnd;
        }
        builder.append(asString.substring(lastSpanEnd));
        return builder;
    }
}