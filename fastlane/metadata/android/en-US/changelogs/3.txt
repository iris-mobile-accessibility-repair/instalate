(beta)
- use Beolingus' json API
- fix Wiktionary link
- minor translation formatting and fastlane changes
